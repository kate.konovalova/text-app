module.exports = {
  pages: {
    index: {
      entry: './src/main.js',
      template: './src/index.pug'
    }
  },
  css: {
        loaderOptions: {
          sass: {
            prependData: '@import "@/assets/styles/styles.scss";'
          }
        }
      }
}